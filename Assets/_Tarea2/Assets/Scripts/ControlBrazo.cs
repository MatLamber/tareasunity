﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlBrazo : MonoBehaviour
{
    [SerializeField] private Animator controlador;

    public void BotonCarpeta()
    {
        if (controlador.GetInteger("GestionarCarpeta") == 0 || controlador.GetInteger("GestionarCarpeta") == -1)
        {
            controlador.SetInteger("GestionarCarpeta", 1);
        }
        else 
        {
            controlador.SetInteger("GestionarCarpeta", -1);
        }
    }
}
