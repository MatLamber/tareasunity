﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TareaGameMan2 : MonoBehaviour
{

    [SerializeField] private GameObject panelMenu;
    [SerializeField] private GameObject nivel;
    [SerializeField] private GameObject[] personajes;
    [SerializeField] private GameObject jugador;
    public static TareaGameMan2 InstanciaTarea2 { get; private set; }
    //Diferentes etapas en las que puede estar el juego
    public enum EstadoDeJuego {MENU, INIT ,PLAY, NIVEL_COMPLETADO, CARGANDO_NIVEL, GAMEOVER}
    //Campo mediante el cual se acceden a los diferentes estados de juego
    private EstadoDeJuego estadoActual;
    //Bandera para controlar si esa efectuando un cambio de estado
    private bool cambioDeEstado;

    private GameObject nivelIns;
    private GameObject personajeIns;
    private GameObject personajeIns1;
    private GameObject jugadorIns;


    void Start()
    {
        InstanciaTarea2 = this;
        CambiarEstado(EstadoDeJuego.MENU);
    }
    
    public void CambiarEstado(EstadoDeJuego nuevoEstado, float retraso = 0)
    {
        StartCoroutine(RetrasoCambio(nuevoEstado, retraso));
    }
    IEnumerator RetrasoCambio (EstadoDeJuego nuevoEstado, float retraso = 0)
    {
        cambioDeEstado = true;
        yield return new WaitForSeconds(retraso);
        TerminarEstado();
        estadoActual = nuevoEstado;
        ComenzarEstado(nuevoEstado);
        cambioDeEstado = false;
    }

    void ComenzarEstado(EstadoDeJuego nuevoEstado)
    {
        switch (nuevoEstado)
        {
           case EstadoDeJuego.MENU :
                panelMenu.SetActive(true);
               break;
           case EstadoDeJuego.INIT :
               jugadorIns = Instantiate(jugador);
               CambiarEstado(EstadoDeJuego.CARGANDO_NIVEL);
               break;
           case EstadoDeJuego.PLAY :
               break;
           case EstadoDeJuego.NIVEL_COMPLETADO :
               break;
           case EstadoDeJuego.CARGANDO_NIVEL :
               nivelIns = Instantiate(nivel);
               personajeIns = Instantiate(personajes[1], new Vector3(-0.14f, -1.8f, -3.47f), Quaternion.AngleAxis( 180f,Vector3.up));
               personajeIns1 = Instantiate(personajes[0], new Vector3(-0.01f, -1.8f, -4.32f), Quaternion.AngleAxis( 180f,Vector3.up));
               CambiarEstado(EstadoDeJuego.PLAY);
               break;
           case  EstadoDeJuego.GAMEOVER :
   
               break;
        }
    }
    void Update()
    {
        switch (estadoActual)
        {
            case EstadoDeJuego.MENU :
                break;
            case EstadoDeJuego.INIT :
                break;
            case EstadoDeJuego.PLAY :

                break;
            case EstadoDeJuego.NIVEL_COMPLETADO :
                break;
            case EstadoDeJuego.CARGANDO_NIVEL :
                break;
            case  EstadoDeJuego.GAMEOVER :
                break;
        }
    }

    void TerminarEstado()
    {
        switch (estadoActual)
        {
            case EstadoDeJuego.MENU :
                panelMenu.SetActive(false);
                break;
            case EstadoDeJuego.INIT :
                break;
            case EstadoDeJuego.PLAY :
                break;
            case EstadoDeJuego.NIVEL_COMPLETADO :
                break;
            case EstadoDeJuego.CARGANDO_NIVEL :
                break;
            case  EstadoDeJuego.GAMEOVER :
                break;
        }
    }

    public void BotonJugar()
    {
        CambiarEstado(EstadoDeJuego.INIT);
    }
}
