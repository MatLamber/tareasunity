﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour
{
    private float _maxX;//Coordenada del limite superior del eje x
    private float _minX;//Coordenada del limite inferior del eje x
    private bool _controlDeLinea = true;//Variable para determinar cuanto baja en el Y cada linea de enemigos
    [SerializeField] private float _velocidad;
    [SerializeField] private float _saltoDeLinea;
    void Start()
    {
        SetearLimitesContenedor();
    }

    // Update is called once per frame
    void Update()
    {
        MovimientoContenedor();
    }
    private void SetearLimitesContenedor()
    {
        //Calcula la distancia entre los enemigos  y la camara
        float distanciaCamara = Vector3.Distance(transform.position, Camera.main.transform.position);
        //Calcula las esquinas inferior de la pantalla en WorldSpace (3D)
        Vector2 esquinasInferiores = Camera.main.ViewportToWorldPoint(new Vector3(0f, 0f, distanciaCamara));
        //Calcula las esquinas superior de la pantalla en WorldSpace (3D)
        Vector2 esquinaSuperiores = Camera.main.ViewportToWorldPoint(new Vector3(1f, 1f, distanciaCamara));
        Bounds borde = GetComponent<Collider>().bounds;
        float ancho = borde.size.x;
        //Bordes laterles
        _maxX = esquinaSuperiores.x - ancho;
        _minX = esquinasInferiores.x + ancho;
    }
    void MovimientoContenedor()
    {
        if (transform.position.x < _maxX && _controlDeLinea) //Los enemigos se mueven a la dercha hasta toparse con el borde de la pantalla
        {
            transform.Translate(1 * _velocidad,0,0);
        }
        else if (transform.position.x >= _maxX && _controlDeLinea) //Cuando llegan al borde bajan en el eje Y, y se mueven del lado opuesto
        {
            transform.Translate(Vector3.down * _saltoDeLinea);
            _controlDeLinea = false;
        }
        if (transform.position.x >_minX  && !_controlDeLinea)
        {
            transform.Translate(-1 * _velocidad,0,0);
        }
        else if (transform.position.x <= _minX  && !_controlDeLinea)
        {
            transform.Translate(Vector3.down * _saltoDeLinea);
            _controlDeLinea = true;
        }
    }
}
