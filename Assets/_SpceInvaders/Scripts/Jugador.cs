﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class Jugador : MonoBehaviour
{
    [SerializeField] private float velocidad;// Variable para cambiar la velocidad a la que se mueve el jugador;
    [SerializeField] private float _disparoDelta ;//Variable que sirve para resetear el tiempo que hay entre disparos
    [SerializeField] private float _sgteDisparo; //Variable para validar el tiempo entre disparos
    [SerializeField] GameObject spawnDeMisil;
    [SerializeField] GameObject misil;
    [SerializeField] private Material _golpeMaterial;
    [SerializeField] private int HP;
    private float _inputHorizontal; // Variable donde se almacena la informacion del input del jugador (eje x)
    private float _inputVertical;   //Vairiable donde se almacena la informacion del input del jugador (eje y)
    private bool _inputMouse;//Vairable donde se almacena la informacion del input del jugador (click de mouse)
    private float _tiempoJugador = 0.0f;//El tiempo que transcurrio desde la ultima vez que el jugador hizo un disparo
    private Rigidbody _rigidbodyComponente;// Variable auxliar por la cual se accede al componente rigidbody del Jugador
    private float _maxX;//Coordenada del limite superior del eje x
    private float _minX;//Coordenada del limite inferior del eje x
    private float _maxY;//Coordenada del limite superior del eje y
    private float _minY;//Coordenada del limite inferior del eje y
    private Material _materialOriginal;
    private MeshRenderer _meshRenderer;
    private bool banderaDisparo = false;

    void Start()
    {
        _rigidbodyComponente = GetComponent<Rigidbody>(); // Se accede al componenete rigidbody 
        _meshRenderer = GetComponent<MeshRenderer>();
        _materialOriginal = _meshRenderer.sharedMaterial;
        SetearLimites();//Se establacen los limites de la pantalla 

    }

    
    void Update()
    {
        _tiempoJugador += Time.deltaTime;//Empieza el contador del tiempo del jugador
        _inputHorizontal = Input.GetAxis("Horizontal");//Se obtiene la informacion de la tecla de movimiento horizontal presionada
        _inputMouse = Input.GetButton("Fire1"); //Se obtiene la informacion del click de mouse
        CalcularLimite(); //Se calcula la posicion del jugador para que no se salga de la pantalla
    }

    private void FixedUpdate()
    {
        _rigidbodyComponente.velocity = new Vector3(_inputHorizontal * velocidad, _rigidbodyComponente.velocity.y, _rigidbodyComponente.velocity.z);
        //_rigidbodyComponente.velocity = new Vector3(_inputHorizontal * velocidad , _inputVertical * velocidad, _rigidbodyComponente.velocity.z);
        
        //Se valida so hubo click de mouse y el jugador esta en el rango de tiempo para efectuar un disparo
        if (_inputMouse && (_tiempoJugador >= _sgteDisparo || banderaDisparo == false))
        {
            
            _sgteDisparo = _tiempoJugador + _disparoDelta;//Se suma la consante al tiempo actual del jugador para poder resetear el tiempo entre cada disparo
            Disparar();
            GameManager.Instancia.Disparo = 0.0f;
            //Debug.Log("Se instancio una bala");
            _sgteDisparo -= _tiempoJugador; // Se reseta la bandera para validar siguiente disparo
            _tiempoJugador = 0.0f; // Se resetea el tiempo del jugador desde el ultimo disparo
            banderaDisparo = true;

        }
        

    }
    
    //Metodo para hallar los limietes de movimiento del juegador con respecto a la pantalla
    private void SetearLimites()
    {
        //Calcula la distancia entre el jugador y la camara
        float distanciaCamara = Vector3.Distance(transform.position, Camera.main.transform.position);
        //Calcula las esquinas inferior de la pantalla en WorldSpace (3D)
        Vector2 esquinasInferiores = Camera.main.ViewportToWorldPoint(new Vector3(0f, 0f, distanciaCamara));
        //Calcula las esquinas superior de la pantalla en WorldSpace (3D)
        Vector2 esquinaSuperiores = Camera.main.ViewportToWorldPoint(new Vector3(1f, 1f, distanciaCamara));
        //Bordes laterales
        _maxX = esquinaSuperiores.x;
        _minX = esquinasInferiores.x;
        //Bordes superior e inferior
        _maxY = esquinaSuperiores.y;
        _minY = esquinasInferiores.y;
    }
    //Metodo para corregir la posicion del jugador en caso de que se salga de los limites
    private void CalcularLimite()
    {
        //Un vector auxiliar para almacer la posicion original del jugador
        Vector3 posicionActual = transform.position;
        //Se valida que el jugador no sobrepase los limites laterales
        posicionActual.x = Mathf.Clamp(posicionActual.x, _minX, _maxX);
        //Se valida que el jugador no sobrepase los limites inferiores y superiore
        posicionActual.y = Mathf.Clamp(posicionActual.y, _minY, _maxY);
        // Se reemplaza la posicion del jugador por la nueva posicion calculada
        transform.position = posicionActual;
    }

    void Disparar()
    {
        //Se instancia el gameObject del misil, el compartamiento del mismo esta en su propio script "Misil.cs"
        Instantiate(misil.transform, spawnDeMisil.transform.position, Quaternion.identity);
        
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.layer == 10)
        {
            HP--;
            GameManager.Instancia.Vidas--;
            _meshRenderer.sharedMaterial = _golpeMaterial;
            Invoke("RestaurarMaterial", 0.1f);
            if (HP <= 0)
            {
                Destroy(this.gameObject);
            }
        }
        
    }

    void RestaurarMaterial()
    {
        _meshRenderer.sharedMaterial = _materialOriginal;
    }
}
