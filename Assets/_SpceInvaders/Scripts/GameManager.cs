﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;
using UnityEngine.UI;
//Sript de un gamemaneger basico que va a server para coordinar las acciones en las diferentes etapas del juego
public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject _jugadorPrefab;//Campo para instanciar al jugador
    [SerializeField] private GameObject _contenedorEnemigos;//Campo para instanciar los niveles
    //Todos los paneles y textos de la UI, tambien un array para los niveles a ser almacenados
    [SerializeField] private Slider vidaRestante;
    [SerializeField] private Slider siguienteDisparo;
    [SerializeField] private Gradient gradienteVida;
    [SerializeField] private RawImage rellenoVida;
    [SerializeField] private RawImage rellenoDisparo;
    [SerializeField] private float cadencia;
    [SerializeField] private Text _textoNivel;
    [SerializeField] private Text _textoPuntuacion;
    [SerializeField] private GameObject[] niveles;  
    [SerializeField] private GameObject _panelMenu;
    [SerializeField] private GameObject _panelPlay;
    [SerializeField] private GameObject _panelGameover;
    [SerializeField] private GameObject _panelNivelCompletado;
    //Se hace la clase GameManager de Instancia Unica para poder poder acceder a ella desde cualquier otra parte del programa
    public static GameManager Instancia { get; private set; }
    private GameObject _nivelActual; // Para tener un tracking de que en nivel se esta actualmente
    private bool _cambioDeEstado; // Bandera para saber si esta cambiando de estado
    
    
    public enum Estado {MENU, INIT ,PLAY, NIVEL_COMPLETADO, CARGANDO_NIVEL, GAMEOVER} // Las diferentes etapas del juego
    private Estado _estado;//Variable para poder operar sobre las etapas del juegoi
    private int _vidas; //Propiedad para tomar cuentas de las vidas restantes del jugador y agregarlas al la UI
    private int _niveles; // Propiedad para tomar en cuenta del nivel actual y agregarla a la UI
    private int _puntuacion;// Propiedad para seguir la puntuacion del jugador
    private float disparo;
    private float tiempoJugador;
    private float tiempoMaximo;
    private bool banderaDisparo = false;
    //Ya que estas se toman como propiuedas se estableces los setter y getters correspondientes
    public int Vidas
    {
        get { return _vidas;}
        set { _vidas = value;
            vidaRestante.value = _vidas;
            rellenoVida.color = gradienteVida.Evaluate(vidaRestante.normalizedValue);
        }
    }

    public int Niveles
    {
        get {return _niveles;} 
        set {_niveles = value;
            _textoNivel.text = "NIVEL: " + ++_niveles;
            _niveles--;
        } 
    }

    public virtual int Puntuacion
    {
        get {return _puntuacion;} 
        set {_puntuacion = value;
            _textoPuntuacion.text = "PUNTUACION: " + _puntuacion;
        } 
    }

    public float Disparo
    {
        get {return disparo;}
        set {disparo = value;}
    }

    //Cuando se da click al boton Play se incia el juego
    public void BotonPlayPresionnado()
    {
        CambiarEstado(Estado.INIT);
    }

    public void BotonMenuPresionado()
    {
        CambiarEstado(Estado.MENU);
    }
    
    void Start()
    {
        Instancia = this; // Se instancia el Gamanager
        CambiarEstado(Estado.MENU); //Aparece el menu
    }

    private void Awake()
    {

    }

    // Una corutina se usa para agregar un retraso minimo entre los cambios de estado del juego
    public void CambiarEstado(Estado nuevoEstado, float retraso = 0)
    {
        StartCoroutine(RetrasoCambio(nuevoEstado, retraso));
    }
    IEnumerator RetrasoCambio (Estado nuevoEstado, float retraso = 0)
    {
        _cambioDeEstado = true;
        yield return new WaitForSeconds(retraso);
        TerminarEstado();
        _estado = nuevoEstado;
        ComenzarEstado(nuevoEstado);
        _cambioDeEstado = false;
    }

    void ComenzarEstado(Estado nuevoEstado)
    {
        switch (nuevoEstado)
        {
            case Estado.MENU:
                Cursor.visible = true; //Se hace visible el cursor en el mundo
                _panelMenu.SetActive(true);
                break;
            case Estado.INIT:
                Cursor.visible = false; // Se esconde el cursor al iniciar el juego
                _panelPlay.SetActive(true);
                Vidas = 5; //UI de vida
                rellenoDisparo.color = gradienteVida.Evaluate(1f);
                Niveles = 0;//Indice para poder iterar sobre el array de nieveles
                Puntuacion = 0;
                Disparo = cadencia;
                tiempoMaximo = cadencia;
                tiempoJugador = tiempoMaximo;
                Instantiate(_jugadorPrefab);// Se instancia al jugador
                CambiarEstado(Estado.CARGANDO_NIVEL);//Se carga el primer nivel
                break;
            case Estado.PLAY:
                break;
            case Estado.NIVEL_COMPLETADO:
                Destroy(_nivelActual); //Se destruye el nivel actual
                Niveles++;//Se aumenta el indice de niveles
                _panelNivelCompletado.SetActive(true);
                CambiarEstado(Estado.CARGANDO_NIVEL, 2f);//Se pasa al estado de cargar nivel
                break;
            case Estado.CARGANDO_NIVEL:
                if (Niveles >= niveles.Length) // Si ya no hay mas niveles
                {
                    CambiarEstado(Estado.GAMEOVER); // Se termina el juego
                }
                else // Si aun quedan niveles
                {
                    _nivelActual = Instantiate(niveles[Niveles]); //se itera sobre el array y se instancia el nuevo nivel
                    CambiarEstado(Estado.PLAY);//se pasa a la etapa de juego
                }
                break;
            case Estado.GAMEOVER:
                Destroy(_nivelActual); //Se destruye el nivel actual durante la pantalla de game over
                _panelGameover.SetActive(true);
                break;
        }
    }
    
    void Update()
    {
        switch (_estado)
        {
            case Estado.MENU:
                break;
            case Estado.INIT:
                break;
            case Estado.PLAY:
                siguienteDisparo.value = CalcularValor();
                rellenoDisparo.color = Color.yellow;
                if (Disparo == 0 && !banderaDisparo)
                {
                    tiempoJugador = 0.0f;
                    banderaDisparo = true;
                }
                if (tiempoJugador <= tiempoMaximo)
                {
                    tiempoJugador += Time.deltaTime;
                }
                if (tiempoJugador >= tiempoMaximo)
                {
                    tiempoJugador = tiempoMaximo;
                    banderaDisparo = false;
                    Disparo = cadencia;
                }
                float CalcularValor()
                {
                    return (tiempoJugador / tiempoMaximo);
                }
                if (Vidas <= 0)
                {
                    CambiarEstado(Estado.GAMEOVER); //Si el jugador se queda sin vidas se pasa a gameover
                }

                if (_nivelActual != null && _nivelActual.transform.childCount == 0 && !_cambioDeEstado)
                {
                    CambiarEstado(Estado.NIVEL_COMPLETADO); // Si ya no quedan naves que desrtuir el jugador supero el nivel
                }
                break;
            case Estado.NIVEL_COMPLETADO:
                break;
            case Estado.CARGANDO_NIVEL:
                break;
            case Estado.GAMEOVER:
                Cursor.visible = true;
                break;
        }
    }

    void TerminarEstado()
    {
        switch (_estado)
        {
            case Estado.MENU:
                _panelMenu.SetActive(false);
                break;
            case Estado.INIT:
                break;
            case Estado.PLAY:
                break;
            case Estado.NIVEL_COMPLETADO:
                _panelNivelCompletado.SetActive(false);
                break;
            case Estado.CARGANDO_NIVEL:
                break;
            case Estado.GAMEOVER:
                _panelPlay.SetActive(false);
                _panelGameover.SetActive(false);
                break;
        }
    }
}
