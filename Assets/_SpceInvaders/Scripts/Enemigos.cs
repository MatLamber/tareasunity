﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Enemigos : MonoBehaviour
{
    [SerializeField] private float _velocidad; //Almacena la velocidad del enemigo
    [SerializeField] private float _saltoDeLinea; //Variable para determinar cuanto baja en el Y cada linea de enemigos
    [SerializeField] private int _golpesRojo; //Resistencia de los enemigos
    [SerializeField] private int _golpesLila;
    [SerializeField] private Material _golpeMaterial; //Material que se renderiza al recibir un impacto de misil
    [SerializeField] private AudioClip _golpeEnemigo; //Sonido del enemigo cuando lo golpean por primera vez
    [SerializeField] private AudioClip _enemigoDestruido; //Sonido del enemigo al ser destruido
    [SerializeField] private GameObject _misilEnemigo;
    [SerializeField] private GameObject _spawnDeMisilEnemigo;
    [SerializeField] private float _cadenciaEnemigo;
    [SerializeField] private int _puntosRojo;
    [SerializeField] private int _puntosLila;
    private bool _controlDeLinea = true; // Bandera para avisar de un cambio de linea
    private Material _materialOriginal; //Variable Auxliar
    private Renderer _renderer; //Variable Auxiliar 
    private bool _enemigoSonido = false; //Bandera que sirve para saber si el enemigo ya fue golpeado una vez
    private AudioSource _audioSource; //Variable para acceder al componente de sonido de los enemigos
    private float reduccion = 0.9f;
    

    void Start()
    {
        _renderer = GetComponent<Renderer>(); //Se accede al componente Renderer
        _materialOriginal = _renderer.sharedMaterial; //Se guarda el material original del gameObject
        _audioSource = GetComponent<AudioSource>(); // Se accede al componente AudioSource
    }

    void Update()
    {

    }

    private void FixedUpdate()
    {
        if (!this.gameObject.tag.Equals("EnemigosLila"))
        {
            Disparar();
        }

    }

    
    //Gestion de colision
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 8) //Si ocurre un colision con un misil
        {
            if (!this.gameObject.tag.Equals("EnemigosLila"))
            {
                _golpesRojo--; //La resistencia se reduce en 1
                GameManager.Instancia.Puntuacion += _puntosRojo;
                if (_golpesRojo <= 0) //Si la resistencia es agotada, se destruye el gameObject
                {
                    
                    _audioSource.PlayOneShot(_enemigoDestruido, 0.1f);
                    Destroy(gameObject, 0.5f);
                    _enemigoSonido = true;

                }

                if (!_enemigoSonido)
                {
                    LeanTween.scale(gameObject, new Vector3(reduccion, reduccion, gameObject.transform.localScale.z), 1.0f * Time.deltaTime);
                    reduccion -= 0.2f;
                    _audioSource.PlayOneShot(_golpeEnemigo, 0.1f);
                    _enemigoSonido = false;
                }

                _renderer.sharedMaterial = _golpeMaterial; //Cuando ocurre una colision se cambia momentaneamte de material
                Invoke("RestaurarMaterial",
                    0.1f); //y luego se restaura, generando asi un efecto de parpadeo como indicador de que hubo colision
                }
            else
            {
                _golpesLila--; //La resistencia se reduce en 1
                GameManager.Instancia.Puntuacion += _puntosLila;
                if (_golpesLila <= 0) //Si la resistencia es agotada, se destruye el gameObject
                {
                    _audioSource.PlayOneShot(_enemigoDestruido, 0.1f);
                    Destroy(gameObject, 0.5f);
                    _enemigoSonido = true;

                }

                if (!_enemigoSonido)
                {
                    _audioSource.PlayOneShot(_golpeEnemigo, 0.1f);
                    _enemigoSonido = false;
                }

                _renderer.sharedMaterial = _golpeMaterial; //Cuando ocurre una colision se cambia momentaneamte de material
                Invoke("RestaurarMaterial",
                    0.1f); //y luego se restaura, generando asi un efecto de parpadeo como indicador de que hubo colision
            }                  
        }
    }
    
    void RestaurarMaterial()
    {
        _renderer.sharedMaterial = _materialOriginal; //Se vuelve a asignar el material original al gameObject
    }


    void Disparar()
    {
        if (Random.Range(0f, _cadenciaEnemigo) < 1)
        {
            //Se instancia el gameObject del misil, el compartamiento del mismo esta en su propio script "Misil.cs"
            Instantiate(_misilEnemigo.transform, _spawnDeMisilEnemigo.transform.position, Quaternion.identity);
        }

    }
}

