﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoTextura : MonoBehaviour
{
    private MeshRenderer _meshRenderer; //Variable por la cual se obtiene acceso al componente MeshRenderer 
    private Material _material;// Variable por la cual se accede al material
    private Vector2 _offset;// Guarda los cambios de posicoin de la textura del material 
    void Start()
    {
        _meshRenderer = GetComponent<MeshRenderer>(); //Se accede al mesh renderer
        _material = _meshRenderer.material; // Se guarda el material (con su textura)
        _offset = _material.mainTextureOffset; // Se almacena la posicoin inicial de la textura
    }

    
    void Update()
    {
        _offset.y += (Time.deltaTime)/2; // La textura se mueve verticalment en factor del tiempo transcurrido, se divide entre dos para hacer que corra mas lento
        _material.mainTextureOffset = _offset; // Se asigna la nueva posicion a la textura
    }
}
