﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BalaEnemigo : MonoBehaviour
{
    [SerializeField] private float _velocidadBala;
    private float _maxY;//Coordenada del limite superior del eje y
    private float _minY;//Coordenada del limite inferior del eje y
    
    void Start()
    {
        SetearLimites();
    }

    private void Update()
    {
        CalcularLimite();
    }

    void FixedUpdate()
    {
        transform.Translate(Vector3.down * Time.deltaTime * _velocidadBala); //Dispara la bala hacia abajo, con una velocidad constante 

    }
    //Encuentra los limites de la pantalla
    private void SetearLimites()
    {
        //Calcula la distancia entre la bala y la camara
        float distanciaCamara = Vector3.Distance(transform.position, Camera.main.transform.position);
        //Calcula las esquinas inferior de la pantalla en WorldSpace (3D)
        Vector2 esquinasInferiores = Camera.main.ViewportToWorldPoint(new Vector3(0f, 0f, distanciaCamara));
        //Calcula las esquinas superior de la pantalla en WorldSpace (3D)
        Vector2 esquinaSuperiores = Camera.main.ViewportToWorldPoint(new Vector3(1f, 1f, distanciaCamara));
        //Bordes superior e inferior
        _maxY = esquinaSuperiores.y;
        _minY = esquinasInferiores.y;
    }
    
    //Valida que el misil se encuentre aun dentro de la pantalla
    private void CalcularLimite()
    {
        //Un vector auxiliar para almacer la posicion original del misil
        Vector3 posicionActual = transform.position;
        //Se valida que la bala sobrepase los superior o inferior
        posicionActual.y = Mathf.Clamp(posicionActual.y, _minY, _maxY);
        // Se destruye la bala cuando llega a los limites de la pantalla
        if (posicionActual.y == _minY)
        {
            Destroy(this.gameObject);
        }
    }
    //Se gestiona lo que pasa al generarse una colision de la bala con el jugador
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 11) //Si la bala choca contra el jugador se destruye la bala
        {
            Destroy(this.gameObject);
        }
    }
}
