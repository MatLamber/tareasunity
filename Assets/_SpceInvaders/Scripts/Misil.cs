﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class Misil : MonoBehaviour
{
    [SerializeField] private float _velocidadMisil;
    private float _maxY;//Coordenada del limite superior del eje y
    private float _minY;//Coordenada del limite inferior del eje y
    
    void Start()
    {
        SetearLimites();
    }

    private void Update()
    {
        CalcularLimite();
    }

    void FixedUpdate()
    {
        transform.Translate(Vector3.up * Time.deltaTime * _velocidadMisil); //Dispara el misil hacia arriba, con una velocidad constante 

    }
    //Encuentra los limites de la pantalla
    private void SetearLimites()
    {
        //Calcula la distancia entre el misil y la camara
        float distanciaCamara = Vector3.Distance(transform.position, Camera.main.transform.position);
        //Calcula las esquinas inferior de la pantalla en WorldSpace (3D)
        Vector2 esquinasInferiores = Camera.main.ViewportToWorldPoint(new Vector3(0f, 0f, distanciaCamara));
        //Calcula las esquinas superior de la pantalla en WorldSpace (3D)
        Vector2 esquinaSuperiores = Camera.main.ViewportToWorldPoint(new Vector3(1f, 1f, distanciaCamara));
        //Bordes superior e inferior
        _maxY = esquinaSuperiores.y;
        _minY = esquinasInferiores.y;
    }
    
    //Valida que el misil se encuentre aun dentro de la pantalla
    private void CalcularLimite()
    {
        //Un vector auxiliar para almacer la posicion original del misil
        Vector3 posicionActual = transform.position;
        //Se valida que el misil no sobrepase los superior o inferior
        posicionActual.y = Mathf.Clamp(posicionActual.y, _minY, _maxY);
        // Se destruye el misil cuando llega a los limites de la pantalla
        if (posicionActual.y == _maxY)
        {
            Destroy(this.gameObject);
        }
    }
    //Se gestiona lo que pasa al generarse una colision del misil con un enemigo
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 9) //Si el misil choca contra un enemigo se destruye la bala
        {
            Destroy(this.gameObject);
        }
    }
}
