﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ContenedorArboles : MonoBehaviour
{
    private int troncosTotales;
    private int troncosVivos;
    private float duracionAnimacion;
   [SerializeField] private AnimationClip animacion;
    void Start()
    {

    }

    private void Awake()
    {
        troncosTotales = gameObject.transform.childCount;
        duracionAnimacion = animacion.length;
        GamaManTarea_1.Instancia.troncoAnimacion1 = duracionAnimacion;
        SacudidoDeCamara.Instance.SacudirCamara(0.3f,duracionAnimacion);
    }


    void Update()
    {
        troncosVivos = gameObject.transform.childCount;
        if (troncosVivos < troncosTotales)
        {
            gameObject.transform.Translate(Vector3.down * 7);
            troncosTotales = troncosVivos;
        }
    }
}
