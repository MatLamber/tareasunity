﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GamaManTarea_1 : MonoBehaviour
{
    [SerializeField] private GameObject nivelCargado; //Campo que hace referencia al nivel actual en ser cargado
    [SerializeField] private GameObject panelMenu;
    [SerializeField] private GameObject panelPlay;
    [SerializeField] private GameObject panelGameOver;
    [SerializeField] private GameObject jugador;
    [SerializeField] private GameObject suelo;
    [SerializeField] private GameObject pajaro;
    [SerializeField] private Text textoScore;
    [SerializeField] private Text highscore;
    //La GameManTarea_1 es de Instancia Unica para poder acceder a ella desde cualquier otro script del juego
    public static GamaManTarea_1 Instancia { get; private set; }
    //Diferentes etapas en las que puede estar el juego
    public enum EstadoDeJuego {MENU, INIT ,PLAY, NIVEL_COMPLETADO, CARGANDO_NIVEL, GAMEOVER}
    //Campo mediante el cual se acceden a los diferentes estados de juego
    private EstadoDeJuego estadoActual;
    //Bandera para controlar si esa efectuando un cambio de estado
    private bool cambioDeEstado;
    //Campo donde se guarda el nivel
    private GameObject nivelActual;
    private GameObject jugadorIns;
    private GameObject sueloIns;
    private GameObject pajaroIns;
    private int score;
    //Propiedad donde se gaurda la duracion de animacion de spawneado de arboles
    private float troncoAnimacion;
    public float troncoAnimacion1
    {
        get {return troncoAnimacion;}
        set {troncoAnimacion = value;}
    }
    // Propiedad para determinar si el jugador golpeo al guardian del arbol
    private bool gameOver;
    public bool gameOver1
    {
        get {return gameOver;}
        set {gameOver = value;} 
    }

    public int score1
    {
        get {return score;}
        set
        {
            score = value;
            textoScore.text = "SCORE: " + score;
        } 
    }

    void Start()
    {
        Instancia = this;
        CambiarEstado(EstadoDeJuego.MENU);
    }
    
    public void CambiarEstado(EstadoDeJuego nuevoEstado, float retraso = 0)
    {
        StartCoroutine(RetrasoCambio(nuevoEstado, retraso));
    }
    IEnumerator RetrasoCambio (EstadoDeJuego nuevoEstado, float retraso = 0)
    {
        cambioDeEstado = true;
        yield return new WaitForSeconds(retraso);
        TerminarEstado();
        estadoActual = nuevoEstado;
        ComenzarEstado(nuevoEstado);
        cambioDeEstado = false;
    }

    void ComenzarEstado(EstadoDeJuego nuevoEstado)
    {
        switch (nuevoEstado)
        {
           case EstadoDeJuego.MENU :
               Cursor.visible = true;
               highscore.text =  "highscore: " + PlayerPrefs.GetInt("highscore",0).ToString();
               panelMenu.SetActive(true);
               break;
           case EstadoDeJuego.INIT :
               gameOver = false;
               score1 = 0;
               sueloIns = Instantiate(suelo);
               jugadorIns = Instantiate(jugador, new Vector3(0f, -7f,8f),Quaternion.identity);
               CambiarEstado(EstadoDeJuego.CARGANDO_NIVEL);
               break;
           case EstadoDeJuego.PLAY :
               panelPlay.SetActive(true);
               break;
           case EstadoDeJuego.NIVEL_COMPLETADO :
               break;
           case EstadoDeJuego.CARGANDO_NIVEL :
               nivelActual = Instantiate(nivelCargado);
               Invoke("InstanciarPajaro",troncoAnimacion1);
               CambiarEstado(EstadoDeJuego.PLAY,troncoAnimacion1);
               break;
           case  EstadoDeJuego.GAMEOVER :
               if (score1 > PlayerPrefs.GetInt("highscore",0))
               {
                   PlayerPrefs.SetInt("highscore", score1);
               }
               Destroy(nivelActual);
               Destroy(jugadorIns);
               Destroy(pajaroIns);
               Destroy(sueloIns);
               panelGameOver.SetActive(true);
               break;
        }
    }
    void Update()
    {
        switch (estadoActual)
        {
            case EstadoDeJuego.MENU :
                break;
            case EstadoDeJuego.INIT :
                break;
            case EstadoDeJuego.PLAY :
                if (gameOver1 == true)
                {
                    CambiarEstado(EstadoDeJuego.GAMEOVER);
                }
                break;
            case EstadoDeJuego.NIVEL_COMPLETADO :
                break;
            case EstadoDeJuego.CARGANDO_NIVEL :
                break;
            case  EstadoDeJuego.GAMEOVER :
                break;
        }
    }

    void TerminarEstado()
    {
        switch (estadoActual)
        {
            case EstadoDeJuego.MENU :
                panelMenu.SetActive(false);
                break;
            case EstadoDeJuego.INIT :
                break;
            case EstadoDeJuego.PLAY :
              
                break;
            case EstadoDeJuego.NIVEL_COMPLETADO :
                break;
            case EstadoDeJuego.CARGANDO_NIVEL :
                break;
            case  EstadoDeJuego.GAMEOVER :
                panelPlay.SetActive(false);
                panelGameOver.SetActive(false);
                break;
        }
    }

    public void BotonStartGame()
    {
        CambiarEstado(EstadoDeJuego.INIT);
    }
    
    public void BotonVolverMenu()
    {
        CambiarEstado(EstadoDeJuego.MENU);
    }

    void InstanciarPajaro()
    {
        pajaroIns = Instantiate(pajaro);
    }
}
