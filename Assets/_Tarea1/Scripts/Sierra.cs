﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sierra : MonoBehaviour
{
    [SerializeField] private float velocidad;
    private float posicionSierra;
    void Start()
    {
        
    }

    void Update()
    {
        posicionSierra = gameObject.transform.position.z;
        if (posicionSierra > 86)
        {
            Destroy(this.gameObject);
        }
    }
    void FixedUpdate()
    {
        transform.Translate(Vector3.forward * (Time.deltaTime * velocidad));
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 13 )
        {
            Destroy(this.gameObject);
        }
         else if (other.gameObject.layer == 15)
        {
            Destroy(other.gameObject);
            GamaManTarea_1.Instancia.gameOver1 = true;
        }
    }
}
