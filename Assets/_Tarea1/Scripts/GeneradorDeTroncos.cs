﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class GeneradorDeTroncos : MonoBehaviour
{
    [SerializeField] private GameObject troncoPrefab;
    [SerializeField] private AnimationClip animacion;
    private GameObject troncoInsntancia;
    private List<GameObject> troncos = new List<GameObject>();
    private int cantidadTroncos;
    private int altura = -71;
    private int profundidad = 86;
    private float duracionAnimacion;
    private int troncosTotales;
    private int troncosVivos;

    void Start()
    {
        
    }
    
    private void Awake()
    {
        cantidadTroncos = Random.Range(6, 10);
        while (troncos.Count < cantidadTroncos)
        {
            troncoInsntancia = Instantiate(troncoPrefab, new Vector3(0, altura, profundidad), troncoPrefab.transform.rotation);
            troncoInsntancia.transform.parent = this.gameObject.transform;
            troncos.Add(troncoInsntancia);
            altura += 7;
        }
        duracionAnimacion = animacion.length;
        troncosTotales = this.gameObject.transform.childCount;
        GamaManTarea_1.Instancia.troncoAnimacion1 = duracionAnimacion;
        SacudidoDeCamara.Instance.SacudirCamara(0.3f,duracionAnimacion);
    }


    void Update()
    {
        troncosVivos = this.gameObject.transform.childCount;
        if ( troncosVivos < troncosTotales)
        {
            this.gameObject.transform.Translate(Vector3.down * 7);
            troncosTotales = troncosVivos;
        }

        if (troncosVivos == 0)
        {
            GamaManTarea_1.Instancia.gameOver1 = true;
        }
    }
    
}
