﻿using System;
using System.Collections;
using System.Collections.Generic;
using Cinemachine;
using UnityEngine;

public class SacudidoDeCamara : MonoBehaviour
{
    public static SacudidoDeCamara Instance { get; private set; }
    private CinemachineVirtualCamera cinemamachineVirtualCamera;
    private float temporizador;
    void Start()
    {
        Instance = this;
    }

    private void Awake()
    {
        cinemamachineVirtualCamera = GetComponent<CinemachineVirtualCamera>();
    }

    public void SacudirCamara(float intensidad, float duracion)
    {
        CinemachineBasicMultiChannelPerlin cinmCinemachineBasicMultiChannelPerlin =
            cinemamachineVirtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

        cinmCinemachineBasicMultiChannelPerlin.m_AmplitudeGain = intensidad;
        temporizador = duracion;
    }

    void Update()
    {
        if (temporizador > 0)
        {
            temporizador -= Time.deltaTime;
            if (temporizador <= 0)
            {
                CinemachineBasicMultiChannelPerlin cinmCinemachineBasicMultiChannelPerlin =
                    cinemamachineVirtualCamera.GetCinemachineComponent<CinemachineBasicMultiChannelPerlin>();

                cinmCinemachineBasicMultiChannelPerlin.m_AmplitudeGain = 0f;
            }
        }
    }
}
