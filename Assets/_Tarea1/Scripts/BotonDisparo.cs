﻿using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using UnityEngine;
using UnityEngine.UIElements;

public class BotonDisparo : MonoBehaviour
{
    [SerializeField] private GameObject sierra;
    private float tiempoJugador = 0.0f;
    private float cadencia = 0.5f;
    private float siguienteDisparo = 0.5f;
    private bool banderaDisparo = false;

    void Start()
    {

    }

    public void GestionDisparo()
    {
        if (tiempoJugador >= cadencia || banderaDisparo == false)
        {
            siguienteDisparo = tiempoJugador + cadencia;
            Disparar();
            siguienteDisparo -= tiempoJugador;
            tiempoJugador = 0.0f;
            banderaDisparo = true;
        }
    }
    void Update()
    {
        tiempoJugador += Time.deltaTime;
    }

    void Disparar()
    {
        Instantiate(sierra, new Vector3(0f, -8f, 8f), Quaternion.identity);
    }
}
