﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tronco : MonoBehaviour
{
    [SerializeField] private Material materialGolpe;
    private Renderer render;
    private Material materialOriginal;
    void Start()
    {
        
    }

    private void Awake()
    {
        render = GetComponent<Renderer>();
        materialOriginal = render.sharedMaterial;
    }

    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.layer == 14)
        {
            render.sharedMaterial = materialGolpe;
            SacudidoDeCamara.Instance.SacudirCamara(0.2f,0.3f);
            Invoke("RestaurarMaterial", 0.05f);
            Invoke("CortarTronco", 0.5f);
        }
    }

    void RestaurarMaterial()
    {
        render.sharedMaterial = materialOriginal;
    }

    void CortarTronco()
    {
        GamaManTarea_1.Instancia.score1 += 200;
        Destroy(this.gameObject);
    }

}
